//
//  ConfigurableCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 16/11/2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

protocol ViewModelItem {
    var reuseIdentifier: String { get }
}

protocol ConfigurableCell {
    func configure(viewModel: ViewModelItem)
}

extension ConfigurableCell {
    func configure(viewModel: ViewModelItem) {}
}
