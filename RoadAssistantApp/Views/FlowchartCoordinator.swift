//
//  FlowchartCoordinator.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 01.08.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

final class FlowchartCoordinator {
    
    private let renderer = FlowchartRenderer()
    private let storyboardForNavigation = UIStoryboard(name: Config.StoryboardIdentifiers.storyboard, bundle: nil)
    private let navigationController: UINavigationController
    private let flowchartNetworkService = FlowchartServiceAssembly.networkService
    
    var flowchart: Flowchart!
    
    init(navigationController: UINavigationController) {
        
        self.navigationController = navigationController
        
        flowchartNetworkService.delegates.addDelegate(delegate: self)
    }
    
    func start() {
        
        let flowchartModel = FlowchartViewModel(flowchart: flowchart, questionNum: flowchart.initial)
        
        guard let destinationVC = storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.flowchartTVC) as? FlowchartTableViewController else { return }
        
        destinationVC.delegate = self
                
        //HIDE BACK BUTTON
        //destinationVC.navigationItem.hidesBackButton = true
        
        navigationController.pushViewController(destinationVC, animated: true)
        
        renderer.render(flowchartViewModel: flowchartModel) { (flowchartModel) in
            destinationVC.flowchartModel = flowchartModel
        }
    }
}

extension FlowchartCoordinator: FlowchartTableViewControllerDelegate {
    
    func flowchartTableViewControllerDidTappedSettingsButton(_ tableViewController: FlowchartTableViewController) {
        
        guard let destinationVC = storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.settingsTVC) as? SettingsTableViewController else { return }
        
        tableViewController.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func flowchartTableViewController(_ viewController: FlowchartTableViewController, didSelectId questionId: Int) {
        
        guard let destinationVC = storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.flowchartTVC) as? FlowchartTableViewController else { return }
        
        let flowchartModel = FlowchartViewModel(flowchart: flowchart, questionNum: questionId)
        
        destinationVC.delegate = self
        
        viewController.navigationController?.pushViewController(destinationVC, animated: true)
        
        renderer.render(flowchartViewModel: flowchartModel) { (flowchartModel) in
            destinationVC.flowchartModel = flowchartModel
        }
    }
}

extension FlowchartCoordinator: FlowchartNetworkServiceDelegate {
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            guard let topViewController = strongSelf.navigationController.topViewController else { return }
            guard topViewController is FlowchartTableViewController else { return }
            
            let tapHandler: () -> Void = {
                strongSelf.navigationController.popToRootViewController(animated: true)
            }
            
            topViewController.showMessage("Новая версия загружена и готова к использованию", type: .success, options: [
                .textColor(.whiteThree),
                .handleTap(tapHandler),
                .textNumberOfLines(0),
                .autoHide(false),
                //.autoHideDelay(3.0),
                .cornerRadius(10.0),
                .position(.top),
                ])
        }
    }
}
