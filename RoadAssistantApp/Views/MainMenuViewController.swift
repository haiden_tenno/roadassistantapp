//
//  ViewController.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 17.06.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var getInstructionsButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var fakeView: UIView!
    
    private lazy var coordinator = FlowchartCoordinator(navigationController: navigationController!)
    
    private let flowchartNetworkService = FlowchartServiceAssembly.networkService
    private let flowchartPersistenceService = FlowchartServiceAssembly.pesristenceService
    private let storyboardForNavigation = UIStoryboard.main
    
    var flowch: Flowchart!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(becomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        #if DEBUG
        let dirDocuments = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        print("Directory:")
        print(dirDocuments)
        #endif
        
        flowchartNetworkService.delegates.addDelegate(delegate: self)
        
        setupView()
        
        let destinationVC = storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.splashNavigationVC) as! UINavigationController

        present(destinationVC, animated: false) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.navigationController?.navigationBar.isHidden = false
            
            strongSelf.fakeView.isHidden = true
        }
    }
    
    @objc func becomeActive() {
        flowch = flowchartPersistenceService.loadFlowchart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
                
        flowch = flowchartPersistenceService.loadFlowchart()

    }
    
    func setupView() {
        
        navigationController?.navigationBar.isHidden = true
        setupTitle()
        setupButton()
        setupInfoLabel()
        setupNavigationBar()
        setupFakeView()
    }
    
    func setupFakeView() {
        fakeView.backgroundColor = .cherry
    }
    
    func setupTitle() {
        
        titleLabel.font = .largeTitle
        titleLabel.textColor = .whiteThree
    }
    
    func setupButton() {
        
        getInstructionsButton.layer.cornerRadius = 10
        getInstructionsButton.layer.masksToBounds = true
        getInstructionsButton.addTarget(self, action: #selector(getInstructionsButtonPressed), for: .touchUpInside)
        getInstructionsButton.setBackgroundColor(color: .getInstructionsGrey, for: .highlighted)
        getInstructionsButton.setTitle("Получить инструкции", for: .normal)
        getInstructionsButton.titleLabel?.font = .headline
        getInstructionsButton.setTitleColor(.whiteThree, for: .normal)
    }
    
    func setupInfoLabel() {
        
        let fullString = NSMutableAttributedString(attributedString: NSAttributedString(string: "Подробное руководство\nот "))
        
        let imageAttachment = NSTextAttachment()
        
        imageAttachment.image = UIImage(named: "dromLogoSplash")
        imageAttachment.bounds = CGRect(x: 0, y: -5, width: 88, height: 20)
        
        let imageString = NSAttributedString(attachment: imageAttachment)
        
        fullString.append(imageString)
        
        infoLabel.attributedText = fullString
        infoLabel.font = .title3
    }
    
    func setupNavigationBar() {
        
        //Back
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //Settings
        let settingsButton = UIButton(type: .system)
        settingsButton.setImage(#imageLiteral(resourceName: "infoIOs").withRenderingMode(.alwaysOriginal), for: .normal)
        settingsButton.imageView?.contentMode = .scaleAspectFit
        settingsButton.addTarget(self, action: #selector(settingsButtonTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: settingsButton)
        //NavBar
        navigationController?.navigationBar.barTintColor = .cherry
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .whiteOne
    }
    
    @objc func getInstructionsButtonPressed(sender: UIButton) {
        
        coordinator.flowchart = flowch
        
        coordinator.start()
    }
    
    @objc func settingsButtonTapped() {
        
        guard let destinationVC = storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.settingsTVC) as? SettingsTableViewController else { return }
        
        navigationController?.pushViewController(destinationVC, animated: true)
    }
    
}

extension MainMenuViewController: FlowchartNetworkServiceDelegate {
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String) {
        
        flowchartPersistenceService.store(flowchart: flowchart, version: version, timestamp: timestamp, date: date)
        
        flowch = flowchartPersistenceService.loadFlowchart()
    }
}
