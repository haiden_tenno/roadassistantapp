//
//  FlowchartDownloadViewController.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 10.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class FlowchartDownloadViewController: UIViewController {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
        
    private lazy var flowchartNetworkService = FlowchartServiceAssembly.networkService
    
    private let flowchartPersistenceService = FlowchartServiceAssembly.pesristenceService
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        flowchartNetworkService.delegates.addDelegate(delegate: self)
        
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        
        progressBar.progress = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        flowchartNetworkService.requestFlowchart()
    }
    
    @objc func cancelButtonPressed() {
        
        flowchartNetworkService.cancelTask()
        
        dismiss(animated: true, completion: nil)
    }
}

extension FlowchartDownloadViewController: FlowchartNetworkServiceDelegate {
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.flowchartPersistenceService.store(flowchart: flowchart, version: version, timestamp: timestamp, date: date)
            
            strongSelf.dismiss(animated: true, completion: nil)
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didReceive error: Error) {
        
        #if DEBUG
        print("Error \(error)")
        #endif
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.dismiss(animated: true, completion: nil)
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, downloaded downloadedBytes: Int, outOf expectedBytes: Int) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.progressBar.progress = SizeHelper().getLoadedPercent(downloaded: downloadedBytes, outOf: expectedBytes)
            
            strongSelf.statusLabel.text = "Загружено \(SizeHelper().getStringSize(from: downloadedBytes)) из \(SizeHelper().getStringSize(from: expectedBytes))"
        }
    }
}

