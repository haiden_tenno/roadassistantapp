//
//  SettingsTableViewManager.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 31.08.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

protocol SettingsTableViewManagerDelegate: class{
    func settingsTableViewManager(_ manager: SettingsTableViewManager, isGoingToPresent activityVC: UIActivityViewController)
    func pressedDownloadButton()
    func pressedCancelButton()
    func removeUpdateSelected()
}

class SettingsTableViewManager: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    weak var delegate: SettingsTableViewManagerDelegate?
    
    private let userDefaults = UserDefaults.standard
    let settingsModel: SettingsViewModel
    
    var footerHeight: CGFloat = 132
    
    init(settingsModel: SettingsViewModel) {
        
        self.settingsModel = settingsModel
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settingsModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsModel.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let item = settingsModel.sections[indexPath.section].items[indexPath.row]
        
        if let item = item as? SettingsViewModelUpdateItem {
            
            item.dowloadCompletionHandler = { [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.settingsModel.changeSettigsViewModelUpdateItem(status: .pullingFlowchart)
                
                strongSelf.delegate?.pressedDownloadButton()
            }
            
            item.cancelCompletionHandler = { [weak self] in
                
                guard let strongSelf = self else { return }

                strongSelf.settingsModel.changeSettigsViewModelUpdateItem(status: .updated)
                
                strongSelf.delegate?.pressedCancelButton()
            }
        }
        
        if let item = item as? SettingsViewModelAutoUpdateItem {
            
            item.switcherCompletionHandler = {
                self.userDefaults.autoUpdatesStatus = !self.userDefaults.autoUpdatesStatus
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? ConfigurableCell {
            cell.configure(viewModel: item)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = settingsModel.sections[indexPath.section].items[indexPath.row]
        
        switch item.type {
         
        #if DEBUG
        case .removeUpdate:
            
            delegate?.removeUpdateSelected()
        #endif
        case .share:
            
            let activityVC = UIActivityViewController(activityItems: Config.itemsToShare, applicationActivities: nil)
            
            delegate?.settingsTableViewManager(self, isGoingToPresent: activityVC)
            
        case .otherApp:
            
            let otherAppItem = item as! SettingsViewModelOtherAppItem
            
            guard let url = URL(string: otherAppItem.app.url) else {

                print("URL error")
                
                tableView.deselectRow(at: indexPath, animated: true)
                
                return
            }
            
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
            
        default:
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let item = settingsModel.sections[section].items[0]
        
        switch item.type {
            
        case .info, .footer:
            return 0
        default:
            return 16
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = settingsModel.sections[indexPath.section].items[indexPath.row]
        
        switch item.type {
        case .footer:
            return footerHeight
        #if DEBUG
        case .removeUpdate:
            return 44
        #endif
        case .share:
            return 44
        default:
            return UITableView.automaticDimension
        }
    }
}
