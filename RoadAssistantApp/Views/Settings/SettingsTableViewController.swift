//
//  SettingsTableViewController.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 14.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
        
    private lazy var settingsTableViewManager = SettingsTableViewManager(settingsModel: settingsModel)
    
    private let flowchartPersistenceService = FlowchartServiceAssembly.pesristenceService
    private let flowchartNetworkService = FlowchartServiceAssembly.networkService
    private let settingsModel = SettingsViewModel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        flowchartNetworkService.delegates.addDelegate(delegate: self)
        
        settingsTableViewManager.delegate = self
        
        tableView.delegate = settingsTableViewManager
        tableView.dataSource = settingsTableViewManager
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if flowchartNetworkService.hasActiveTask() {
            settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .pullingFlowchart)
        } else {
            flowchartNetworkService.requestVersion()
            settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .pullingVersion)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if settingsTableViewManager.settingsModel.getSettingsViewModelUpdateItemStatus() == .pullingVersion {
            flowchartNetworkService.cancelTask()
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        tableView.layoutIfNeeded()
        
        var footerHeight: CGFloat = settingsTableViewManager.footerHeight
        
        if view.frame.height - tableView.contentSize.height + footerHeight > 132 {
            
            footerHeight = view.frame.height - tableView.contentSize.height + footerHeight
        }
        
        if Int(footerHeight) != Int(settingsTableViewManager.footerHeight) {
            
            settingsTableViewManager.footerHeight = footerHeight
            tableView.reloadData()
        }
    }
}

extension SettingsTableViewController: FlowchartNetworkServiceDelegate {
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .updated)
            
            strongSelf.tableView.reloadData()            
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad version: String, timestamp: Int, size: Int) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            if strongSelf.settingsTableViewManager.settingsModel.getSettingsViewModelUpdateItemStatus() == .pullingVersion {
                
                if TimestampHelper().checkIfDeviceTimestampOlder(than: timestamp) {
                    
                    strongSelf.settingsTableViewManager.settingsModel.changeSettingsViewModelUpdateItem(updateSize: size, and: nil)
                    
                    strongSelf.settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .newUpdate)
                } else {
                    
                    strongSelf.settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .noUpdate)
                }
                
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didReceive error: Error) {
        
        print("Error \(error)")
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            if strongSelf.settingsTableViewManager.settingsModel.getSettingsViewModelUpdateItemStatus() != .updated {

                strongSelf.settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .error)
            }
            
            strongSelf.tableView.reloadData()
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, downloaded downloadedBytes: Int, outOf expectedBytes: Int) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.settingsTableViewManager.settingsModel.changeSettingsViewModelUpdateItem(updateSize: expectedBytes, and: downloadedBytes)
            
            strongSelf.tableView.reloadData()
        }
    }
}

extension SettingsTableViewController: SettingsTableViewManagerDelegate {
    
    func settingsTableViewManager(_ manager: SettingsTableViewManager, isGoingToPresent activityVC: UIActivityViewController) {
        
        activityVC.popoverPresentationController?.sourceView = view
        
        present(activityVC, animated: true, completion: nil)
    }
    
    func pressedDownloadButton() {
        
        tableView.reloadData()
        
        flowchartNetworkService.requestFlowchart()
    }
    
    func pressedCancelButton() {
        
        flowchartNetworkService.cancelTask()
    }
    
    func removeUpdateSelected() {
        
        DispatchQueue.main.async { [weak self] in
            
            guard let strongSelf = self else { return }
            
            guard strongSelf.flowchartPersistenceService.fileExists() else { return }
            
            let alert = UIAlertController(title: "Удалить обновление?", message: "Загруженная схема будут удалена.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { (action: UIAlertAction!) in
                
                strongSelf.flowchartPersistenceService.removeUpdates()
                
                strongSelf.flowchartNetworkService.requestVersion()

                strongSelf.settingsTableViewManager.settingsModel.changeSettigsViewModelUpdateItem(status: .pullingVersion)
                
                strongSelf.tableView.reloadData()
            }))
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            strongSelf.present(alert, animated: true, completion: nil)
        }
    }
}
