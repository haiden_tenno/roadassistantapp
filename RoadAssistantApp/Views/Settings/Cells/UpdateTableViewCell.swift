//
//  UpdateTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class UpdateTableViewCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var cancelButton: UIButton!
    
    weak var viewModel: SettingsViewModelUpdateItem?
    
    private let userDefaults = UserDefaults.standard
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectionStyle = .none
        
        activityInd.hidesWhenStopped = true
        
        dateLabel.font = .body
        dateLabel.textColor = .blackOne
        
        sizeLabel.font = .caption1
        sizeLabel.textColor = .warmGrey
        
        setupButtons()
    }
    
    func setupButtons() {
        
        downloadButton.addTarget(self, action: #selector(downloadButtonPressed), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    func configure(viewModel: ViewModelItem) {
        
        guard let viewModel = viewModel as? SettingsViewModelUpdateItem else { return }
        guard let dateOfBase = userDefaults.dateOfBase else { return }
        guard let deviceVersion = userDefaults.appVersion else { return }
        
        dateLabel.text = "База инструкций от " + dateOfBase
        sizeLabel.text = "Версия: \(deviceVersion)"
        
        self.viewModel = viewModel
        
        switch viewModel.status {
        case .pullingVersion:
            
            activityInd.startAnimating()
            
            downloadButton.isHidden = true
            
            cancelButton.isHidden = true
            
            sizeLabel.text = "Проверка обновлений..."
            
            progressView.isHidden = true
            
        case .pullingFlowchart:
            
            activityInd.stopAnimating()
            
            downloadButton.isHidden = true
            
            cancelButton.isHidden = false
            
            progressView.isHidden = false
            
            dateLabel.text = "Загрузка обновления"
            
            sizeLabel.text = "Загружено \(SizeHelper().getStringSize(from: viewModel.downloadedBytes)) из \(SizeHelper().getStringSize(from: viewModel.updateSize))"
            
            progressView.progress = SizeHelper().getLoadedPercent(downloaded: viewModel.downloadedBytes, outOf: viewModel.updateSize)
        case .newUpdate:
            
            activityInd.stopAnimating()
            
            downloadButton.isHidden = false
            
            cancelButton.isHidden = true
            
            let updateSizeStr = SizeHelper().getStringSize(from: viewModel.updateSize)
            
            sizeLabel.text = "Доступны обновления, \(updateSizeStr)"
            
            progressView.isHidden = true
            
        case .updated:
            
            activityInd.stopAnimating()
            
            downloadButton.isHidden = true
            
            cancelButton.isHidden = true
            
            sizeLabel.text = "Обновления установлены"
            
            progressView.isHidden = true
            
        case .noUpdate:
            
            activityInd.stopAnimating()
            
            downloadButton.isHidden = true
            
            cancelButton.isHidden = true
            
            progressView.isHidden = true
            
        case .error:
            
            activityInd.stopAnimating()
            
            downloadButton.isHidden = true
            
            cancelButton.isHidden = true
            
            sizeLabel.text = "Невозможно подключиться к серверу обновлений"
            
            progressView.isHidden = true
        }
    }
    
    @objc func downloadButtonPressed(sender: UIButton) {
        
        guard let viewModel = viewModel else { return }
        guard let dowloadCompletionHandler = viewModel.dowloadCompletionHandler else { return }
        dowloadCompletionHandler()
    }
    
    @objc func cancelButtonPressed(sender: UIButton) {
        
        guard let viewModel = viewModel else { return }
        guard let cancelCompletionHandler = viewModel.cancelCompletionHandler else { return }
        cancelCompletionHandler()
    }
}
