//
//  AutoUpdatesTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class AutoUpdatesTableViewCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var updateSwitcher: UISwitch!
    @IBOutlet weak var infoLabel: UILabel!
    
    private let userDefaults = UserDefaults.standard
    
    weak var viewModel: SettingsViewModelAutoUpdateItem?
        
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectionStyle = .none
        
        infoLabel.text = "Автоматическое обновление"
        infoLabel.font = .body
        infoLabel.textColor = .blackOne
        
        updateSwitcher.addTarget(self, action: #selector(autoUpdateSwitcherUsed), for: .touchUpInside)
    }
    
    func configure(viewModel: ViewModelItem) {
       
        guard let viewModel = viewModel as? SettingsViewModelAutoUpdateItem else { return }
        
        if userDefaults.autoUpdatesStatus {
            updateSwitcher.isOn = true
        } else {
            updateSwitcher.isOn = false
        }
        
        //switcherCompletionHandler = viewModel.switcherCompletionHandler
        self.viewModel = viewModel
    }
    
    @objc func autoUpdateSwitcherUsed(sender: UISwitch) {
        
        guard let viewModel = viewModel else { return }
        guard let switcherCompletionHandler = viewModel.switcherCompletionHandler else { return }
        switcherCompletionHandler()
    }
}
