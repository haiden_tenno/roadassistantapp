//
//  OtherAppTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class OtherAppTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var appLogoImageView: UIImageView!
    @IBOutlet weak var appTitleLabel: UILabel!
    @IBOutlet weak var appInfoLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        appTitleLabel.font = .body
        appTitleLabel.textColor = .blackOne
        
        appInfoLabel.font = .caption1
        appInfoLabel.textColor = .blackOne
    }
    
    func configure(viewModel: ViewModelItem) {
        
        guard let viewModel = viewModel as? SettingsViewModelOtherAppItem else { return }
        
        appLogoImageView.image = UIImage(named: viewModel.app.logoFileName)
        
        appTitleLabel.text = viewModel.app.title
        
        appInfoLabel.text = viewModel.app.info
    }
}
