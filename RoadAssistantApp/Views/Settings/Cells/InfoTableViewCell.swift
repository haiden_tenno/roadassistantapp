//
//  InfoTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectionStyle = .none
        
        titleLabel.text = "Дром ДТП"
        titleLabel.font = .headline
        titleLabel.textColor = .blackOne
        
        infoLabel.font = .body
        infoLabel.textColor = .blackOne
        infoLabel.text = "Быстрая справка на случай\nдорожно-транспортных\nпроисшествий"
    }
}
