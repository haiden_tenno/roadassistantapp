//
//  RemoveUpdateTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 03/11/2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

#if DEBUG
import UIKit

class RemoveUpdateTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        infoLabel.text = "Удалить обновление"
        infoLabel.font = .body
        infoLabel.textColor = .blackOne
    }
}
#endif
