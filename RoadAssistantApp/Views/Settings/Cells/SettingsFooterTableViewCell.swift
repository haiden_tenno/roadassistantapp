//
//  SettingsFooterTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 22.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class SettingsFooterTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var footerTextLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectionStyle = .none
        
        backgroundColor = .footerGrey
        
        footerTextLabel.text = Config.Strings.footerLabel
        footerTextLabel.font = .caption1
    }
}
