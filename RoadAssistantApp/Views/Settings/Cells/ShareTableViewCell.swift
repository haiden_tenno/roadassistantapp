//
//  ShareTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class ShareTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        infoLabel.text = "Рассказать друзьям"
        infoLabel.font = .body
        infoLabel.textColor = .blackOne
    }
}
