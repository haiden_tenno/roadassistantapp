//
//  UpdateViewController.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 26.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class UpdateViewController: UIViewController {

    @IBOutlet weak var updateSizeLabel: UILabel!
    @IBOutlet weak var installUpdateButton: UIButton!
    @IBOutlet weak var dontInstallUpdateButton: UIButton!
    
    private let storyboardForNavigation = UIStoryboard.main
    
    var updateSize: Int!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView() {
        
        //Button
        installUpdateButton.layer.cornerRadius = 15
        installUpdateButton.addTarget(self, action: #selector(installUpdateButtonPressed), for: .touchUpInside)
        dontInstallUpdateButton.addTarget(self, action: #selector(dontInstallUpdateButtonPressed), for: .touchUpInside)
        
        //Label
        let fullString = NSMutableAttributedString(attributedString: updateSizeLabel.attributedText!)
        fullString.append(NSAttributedString(string: SizeHelper().getStringSize(from: updateSize)))
        
        updateSizeLabel.attributedText = fullString
        
    }
    
    @objc func installUpdateButtonPressed() {
 
        guard let destinationVC = storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.download) as? FlowchartDownloadViewController else { return }
        
        navigationController?.pushViewController(destinationVC, animated: false)
    }
    
    @objc func dontInstallUpdateButtonPressed() {

        dismiss(animated: true, completion: nil)
    }
}
