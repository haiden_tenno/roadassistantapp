//
//  StartViewController.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 02.08.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class StartViewController: UIViewController  {
        
    private lazy var flowchartNetworkService = FlowchartServiceAssembly.networkService
    
    private let userDefaults = UserDefaults.standard
    private let storyboardForNavigation = UIStoryboard.main
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        flowchartNetworkService.delegates.addDelegate(delegate: self)
        
        if userDefaults.autoUpdatesStatus {
            flowchartNetworkService.requestVersion()
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}

extension StartViewController: FlowchartNetworkServiceDelegate {
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad version: String, timestamp: Int, size: Int) {
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            if TimestampHelper().checkIfDeviceTimestampOlder(than: timestamp) {
                guard let destinationVC = strongSelf.storyboardForNavigation.instantiateViewController(withIdentifier: Config.StoryboardIdentifiers.updateVC) as? UpdateViewController else { return }
                
                destinationVC.updateSize = size
                
                strongSelf.navigationController?.pushViewController(destinationVC, animated: true)
            } else {
                strongSelf.navigationController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didReceive error: Error) {
        
        #if DEBUG
        print("Error \(error)")
        #endif
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}
