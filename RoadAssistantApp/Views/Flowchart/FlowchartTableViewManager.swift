//
//  FlowchartTableViewManager.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 31.08.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

protocol FlowchartTableViewManagerDelegate: class {
    func flowchartTableViewManager(_ manager: FlowchartTableViewManager, didSelectId questionId: Int)
}

class FlowchartTableViewManager: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    weak var delegate: FlowchartTableViewManagerDelegate?
    
    var flowchartModel: FlowchartViewModel?
    var tableView: UITableView?
    var footerHeight: CGFloat = 112
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let flowchartModel = flowchartModel else { return 0 }
        
        return flowchartModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let flowchartModel = flowchartModel else { return UITableViewCell() }
        
        let item = flowchartModel.items[indexPath.row]
        
        if let item = item as? FlowchartViewModelAnswerItem {
            
            item.selectionHandler = {
                
                self.delegate?.flowchartTableViewManager(self, didSelectId: item.endpoint.to)
            }
        }
        
        if let item = item as? FlowchartViewModelFunctionItem {
            
            item.selectionHandler = {
                
                //FUNCTION ACTION
                print(item.action.substituteMessage)
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.reuseIdentifier, for: indexPath)
        
        if let cell = cell as? ConfigurableCell {
            cell.configure(viewModel: item)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let flowchartModel = flowchartModel else { return }
        
        let item = flowchartModel.items[indexPath.row]
        
        switch item.type {
            
        case .showDescription:
            
            flowchartModel.changeStatus()
            
            var descriptionCellIndex = indexPath
            
            descriptionCellIndex.row -= 1
            
            tableView.reloadRows(at: [descriptionCellIndex, indexPath], with: .fade)
            
            if flowchartModel.getStatus() {
                tableView.scrollToRow(at: descriptionCellIndex, at: .top, animated: true)
            } else {
                tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
            
        default:
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let flowchartModel = flowchartModel else { return 0 }
        
        let item = flowchartModel.items[indexPath.row]
        
        switch item.type {
            
        case .description:
            
            let descriptionItem = item as! FlowchartViewModelDescriptionItem
            
            if descriptionItem.status == false {
                return 0
            } else {
                return UITableView.automaticDimension
            }
            
        case .showDescription:
            
            return 40
            
        case .footer:
            
            return footerHeight
            
        default:
            
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {

        guard let flowchartModel = flowchartModel else { return 0 }
        
        if flowchartModel.items.contains(where: { item in
            
            if item is FlowchartViewModelFooterItem {
                return true
            }
            
            return false
        }) {
            return 0
        }
        
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        
        headerView.backgroundColor = UIColor.clear
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView()
        
        footerView.backgroundColor = UIColor.clear
        
        return footerView
    }
}
