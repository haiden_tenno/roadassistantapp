//
//  FlowchartFooterTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 21.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class FlowchartFooterTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var footerTextLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectionStyle = .none
        
        footerTextLabel.text = Config.Strings.footerLabel
        footerTextLabel.font = .caption1
        
//        layer.borderWidth = 1
    }
}
