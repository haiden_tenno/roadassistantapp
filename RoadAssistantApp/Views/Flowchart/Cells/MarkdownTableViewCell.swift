//
//  MarkdownTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 23.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

enum MarkdownTableViewCellSpecialization {
    case question
    case description
}

class MarkdownTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var mdTextView: UITextView!
    
    var specialization: MarkdownTableViewCellSpecialization!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        backgroundColor = .none
        
        selectionStyle = .none
        
        mdTextView.isSelectable = true
        mdTextView.dataDetectorTypes = .link
        
//        layer.borderWidth = 1
    }
    
    func configure(viewModel: ViewModelItem) {
        
        if let viewModel = viewModel as? FlowchartViewModelQuestionItem {
            
            specialization = .question
            mdTextView.attributedText = viewModel.renderedString
        }
        
        if let viewModel = viewModel as? FlowchartViewModelDescriptionItem {
            specialization = .description
            mdTextView.attributedText = viewModel.renderedString
        }
    }
}
