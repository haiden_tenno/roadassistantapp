//
//  ShowDescriptionTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 28.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class ShowDescriptionTableViewCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var showLabel: UILabel!
    @IBOutlet weak var showIcon: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        backgroundColor = .none
        selectionStyle = .none
        
        showLabel.font = UIFont.footnote
        
        showIcon.frame = CGRect(x: 0, y: 0, width: 11, height: 9)
        showIcon.contentMode = .scaleAspectFit
        
//        layer.borderWidth = 1
    }
    
    func configure(viewModel: ViewModelItem) {
        
        guard let viewModel = viewModel as? FlowchartViewModelShowDescriptionItem else { return }
        
        if viewModel.status {
            showLabel.text = "Скрыть примечание"
            showLabel.textColor = UIColor.blackOne
            
            showIcon.image = #imageLiteral(resourceName: "arrowChevronUp")
        } else {
            showLabel.text = "Показать примечание"
            showLabel.textColor = UIColor.warmGrey
            
            showIcon.image = #imageLiteral(resourceName: "arrowChevronDown")
        }
    }
}
