//
//  ActionTableViewCell.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 20.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

class ActionTableViewCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var actionButton: UIButton!
    
    weak var viewModel: FlowchartViewModelActionItem?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        selectionStyle = .none
        
        setupButton()
        
//        layer.borderWidth = 1
    }
    
    func setupButton() {
        
        actionButton.layer.cornerRadius = 10
        actionButton.titleLabel?.font = .headline
        actionButton.setTitleColor(.waterBlue, for: .normal)
        actionButton.setBackgroundColor(color: .whiteOne, for: .normal)
        actionButton.setBackgroundColor(color: .pinkishGrey, for: .highlighted)
        actionButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
    }
    
    func configure(viewModel: ViewModelItem) {
        
        guard let viewModel = viewModel as? FlowchartViewModelActionItem else { return }
        
        self.viewModel = viewModel
        
        if let viewModel = viewModel as? FlowchartViewModelAnswerItem {
            
            actionButton.setTitle(viewModel.answer.label, for: .normal)            
        }
        
        if let viewModel = viewModel as? FlowchartViewModelFunctionItem {
            
            actionButton.setTitle(viewModel.action.id, for: .normal)
        }
    }
    
    @objc func buttonPressed(sender: UIButton) {
        
        guard let viewModel = viewModel else { return }
        guard let selectionHandler = viewModel.selectionHandler else { return }
        selectionHandler()
    }
}
