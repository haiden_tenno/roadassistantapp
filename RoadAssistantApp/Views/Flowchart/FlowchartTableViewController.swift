//
//  FlowchartTableViewController.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 22.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

protocol FlowchartTableViewControllerDelegate: class {
    func flowchartTableViewController(_ tableViewController: FlowchartTableViewController, didSelectId questionId: Int)
    func flowchartTableViewControllerDidTappedSettingsButton(_ tableViewController: FlowchartTableViewController)
}

class FlowchartTableViewController: UITableViewController {
    
    weak var delegate: FlowchartTableViewControllerDelegate?
    
    private let activityIndicator = UIActivityIndicatorView(style: .gray)
    
    var flowchartModel: FlowchartViewModel? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.activityIndicator.stopAnimating()
                
                strongSelf.flowchartTableViewManager.flowchartModel = strongSelf.flowchartModel
                
                strongSelf.tableView.reloadData()
            }
        }
    }
    var flowchartTableViewManager = FlowchartTableViewManager()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        setupNavigationBar()
        
        flowchartTableViewManager.delegate = self
        flowchartTableViewManager.tableView = tableView
        
        tableView.delaysContentTouches = false
        tableView.delegate = flowchartTableViewManager
        tableView.dataSource = flowchartTableViewManager
        
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        guard let flowchartModel = flowchartModel else { return }
        
        if tableView.numberOfRows(inSection: 0) != 0 {
            
            tableView.layoutIfNeeded()
            
            if flowchartModel.final {
                
                var footerHeight: CGFloat = flowchartTableViewManager.footerHeight
                
                if view.frame.height - tableView.contentSize.height + footerHeight > 112 {
                    
                    footerHeight = view.frame.height - tableView.contentSize.height + footerHeight
                } else {
                    footerHeight = 112
                }
                
                if Int(footerHeight) != Int(flowchartTableViewManager.footerHeight) {
                    
                    flowchartTableViewManager.footerHeight = footerHeight
                    tableView.reloadData()
                }
            }
        }
    }
    
    private func setupNavigationBar() {
        
        navigationItem.rightBarButtonItems = []
        
        //Settings
        let settingsButton = UIButton(type: .system)
        settingsButton.setImage(#imageLiteral(resourceName: "infoIOs").withRenderingMode(.alwaysOriginal), for: .normal)
        settingsButton.imageView?.contentMode = .scaleAspectFit
        settingsButton.addTarget(self, action: #selector(settingsButtonTapped), for: .touchUpInside)
        navigationItem.rightBarButtonItems?.append(UIBarButtonItem(customView: settingsButton))
        //Back
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //NavBar
        navigationController?.navigationBar.barTintColor = .cherry
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .whiteOne
    }
    
    @objc func settingsButtonTapped(sender: UIButton) {
        
        delegate?.flowchartTableViewControllerDidTappedSettingsButton(self)
    }
}

extension FlowchartTableViewController: FlowchartTableViewManagerDelegate {
    
    func flowchartTableViewManager(_ manager: FlowchartTableViewManager, didSelectId questionId: Int) {
        
        guard let delegate = delegate else { return }
        
        delegate.flowchartTableViewController(self, didSelectId: questionId)
    }
}
