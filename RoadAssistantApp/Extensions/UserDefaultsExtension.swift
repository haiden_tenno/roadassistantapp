//
//  UserDefaultsExtension.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 19.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

extension UserDefaults {
    
    var appVersion: String? {
        get {
            return string(forKey: Config.UserDefaults.version)
        }

        set(version) {
            set(version, forKey: Config.UserDefaults.version)
        }
    }

    var appTimestamp: Int? {
        get {
            return integer(forKey: Config.UserDefaults.timestamp)
        }

        set(timestamp) {
            set(timestamp, forKey: Config.UserDefaults.timestamp)
        }
    }

    var dateOfBase: String? {
        get {
            return string(forKey: Config.UserDefaults.date)
        }
        
        set(date) {
            set(date, forKey: Config.UserDefaults.date)
        }
    }
    
    var autoUpdatesStatus: Bool {
        get {
            return bool(forKey: Config.UserDefaults.autoUpdates)
        }

        set(status) {
            set(status, forKey: Config.UserDefaults.autoUpdates)
        }
    }
    
    var storedFlowchartVersion: String? {
        get {
            return string(forKey: Config.UserDefaults.storedFlowchartVersion)
        }

        set(version) {
            set(version, forKey: Config.UserDefaults.storedFlowchartVersion)
        }
    }
    
    var storedFlowchartTimestamp: Int? {
        get {
            return integer(forKey: Config.UserDefaults.storedFlowchartTimestamp)
        }
        
        set(timestamp) {
            set(timestamp, forKey: Config.UserDefaults.storedFlowchartTimestamp)
        }
    }
}

