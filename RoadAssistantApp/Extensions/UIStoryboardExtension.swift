//
//  UIStoryboardExtension.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 01.08.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    static let main = UIStoryboard(name: Config.StoryboardIdentifiers.storyboard, bundle: nil)
}
