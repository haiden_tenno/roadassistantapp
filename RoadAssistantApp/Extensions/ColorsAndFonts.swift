//
//  ColorsAndFonts.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 03.08.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit

// Color palette
extension UIColor {
    @nonobjc class var blackOne: UIColor {
        return UIColor(white: 0.0, alpha: 1.0)
    }
    @nonobjc class var cherry: UIColor {
        return UIColor(red: 219.0 / 255.0, green: 0.0, blue: 27.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var waterBlue: UIColor {
        return UIColor(red: 27.0 / 255.0, green: 117.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteOne: UIColor {
        return UIColor(white: 232.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var warmGrey: UIColor {
        return UIColor(white: 139.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteThree: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
    @nonobjc class var pinkishGrey: UIColor {
        return UIColor(white: 193.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var whiteTwo: UIColor {
        return UIColor(white: 245.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var footerGrey: UIColor {
        return UIColor(red: 239.0 / 255.0, green: 239.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var getInstructionsGrey: UIColor {
        return UIColor(red: 28.0 / 255.0, green: 28.0 / 255.0, blue: 28.0 / 255.0, alpha: 1.0)
    }
}

// Text styles
extension UIFont {
    
    class var largeTitle: UIFont {
        return UIFont.systemFont(ofSize: 34.0, weight: .regular)
    }
    
    class var title2: UIFont {
        return UIFont.systemFont(ofSize: 22.0, weight: .regular)
    }
    
    class var materialLightTitle: UIFont {
        return UIFont(name: "Roboto-Medium", size: 20.0)!
    }
    
    class var title3: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .regular)
    }
    
    class var headline: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .semibold)
    }
    
    class var body: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .regular)
    }
    
    class var footnote: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: .regular)
    }
    
    class var caption1: UIFont {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular)
    }
}
