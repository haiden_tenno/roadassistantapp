//
//  SettingsViewModel.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

enum SettingsViewModelItemType {
    case info
    case update
    #if DEBUG
    case removeUpdate
    #endif
    case autoUpdate
    case share
    case otherApp
    case footer
}

protocol SettingsViewModelItem: ViewModelItem {
    var type: SettingsViewModelItemType { get }
}

struct SettingsViewModelSection {
    var items: [SettingsViewModelItem]
}

final class SettingsViewModelInfoItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "InfoCell"
    }
    
    var type: SettingsViewModelItemType {
        return .info
    }
}

enum SettingsViewModelUpdateItemStatus {
    case pullingVersion
    case pullingFlowchart
    case updated
    case newUpdate
    case noUpdate
    case error
}

final class SettingsViewModelUpdateItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "UpdateCell"
    }
    
    var type: SettingsViewModelItemType {
        return .update
    }
    
    var status: SettingsViewModelUpdateItemStatus = .pullingVersion
    var updateSize: Int = 0
    var downloadedBytes: Int = 0
    
    var dowloadCompletionHandler: (() -> Void)?
    var cancelCompletionHandler: (() -> Void)?
}

#if DEBUG
final class SettingsViewModelRemoveUpdateItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "RemoveUpdateCell"
    }
    
    var type: SettingsViewModelItemType {
        return .removeUpdate
    }
}
#endif

final class SettingsViewModelAutoUpdateItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "AutoUpdatesCell"
    }
    
    var type: SettingsViewModelItemType {
        return .autoUpdate
    }
    
    var switcherCompletionHandler: (() -> Void)?
}

final class SettingsViewModelShareItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "ShareCell"
    }
    
    var type: SettingsViewModelItemType {
        return .share
    }
}

final class SettingsViewModelOtherAppItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "OtherAppCell"
    }
    
    var type: SettingsViewModelItemType {
        return .otherApp
    }
    
    var app: OtherApp
    
    init(app: OtherApp) {
        self.app = app
    }
}

final class SettingsViewModelFooterItem: SettingsViewModelItem {
    
    var reuseIdentifier: String {
        return "SettingsFooterCell"
    }
    
    var type: SettingsViewModelItemType {
        return .footer
    }
}

final class SettingsViewModel: NSObject {
    
    var sections = [SettingsViewModelSection]()
    
    override init() {
        
        super.init()
        
        var items = [SettingsViewModelItem]()
        
        let info = SettingsViewModelInfoItem()
        items.append(info)
        let infoSection = SettingsViewModelSection(items: items)
        sections.append(infoSection)
        items.removeAll()
        
        let update = SettingsViewModelUpdateItem()
        items.append(update)
        let updateSection = SettingsViewModelSection(items: items)
        sections.append(updateSection)
        items.removeAll()
        
        #if DEBUG
        let removeUpdate = SettingsViewModelRemoveUpdateItem()
        items.append(removeUpdate)
        let removeUpdateSection = SettingsViewModelSection(items: items)
        sections.append(removeUpdateSection)
        items.removeAll()
        #endif
        
        let autoUpdates = SettingsViewModelAutoUpdateItem()
        items.append(autoUpdates)
        let autoUpdatesSection = SettingsViewModelSection(items: items)
        sections.append(autoUpdatesSection)
        items.removeAll()
        
        let share = SettingsViewModelShareItem()
        items.append(share)
        let shareSection = SettingsViewModelSection(items: items)
        sections.append(shareSection)
        items.removeAll()
        
        for app in Config.otherApps {
            let otherApps = SettingsViewModelOtherAppItem(app: app)
            items.append(otherApps)
        }
        let otherAppsSection = SettingsViewModelSection(items: items)
        sections.append(otherAppsSection)
        items.removeAll()
        
        let footer = SettingsViewModelFooterItem()
        items.append(footer)
        let footerSection = SettingsViewModelSection(items: items)
        sections.append(footerSection)
    }
    
    func changeSettigsViewModelUpdateItem(status: SettingsViewModelUpdateItemStatus) {
        
        for section in sections {
            if section.items is [SettingsViewModelUpdateItem] {
                guard let item = section.items[0] as? SettingsViewModelUpdateItem else { return }
                item.status = status
                break
            }
        }
    }
    
    func getSettingsViewModelUpdateItemStatus() -> SettingsViewModelUpdateItemStatus? {
        
        for section in sections {
            if section.items is [SettingsViewModelUpdateItem] {
                guard let item = section.items[0] as? SettingsViewModelUpdateItem else { return nil }
                return item.status
            }
        }
        return nil
    }
    
    func changeSettingsViewModelUpdateItem(updateSize: Int?, and downloadedBytes: Int?) {
        
        for section in sections {
            if section.items is [SettingsViewModelUpdateItem] {
                guard let item = section.items[0] as? SettingsViewModelUpdateItem else { return }
                
                if let updateSize = updateSize {
                    item.updateSize = updateSize
                }
                
                if let downloadedBytes = downloadedBytes {
                    item.downloadedBytes = downloadedBytes
                }
                
                break
            }
        }
    }
}

final class OtherApp {
    
    var title: String
    var info: String
    var logoFileName: String
    var url: String
    
    init(title: String, info: String, logoFileName: String, url: String) {
        
        self.title = title
        self.info = info
        self.logoFileName = logoFileName
        self.url = url
    }
}
