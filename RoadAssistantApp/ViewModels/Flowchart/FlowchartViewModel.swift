//
//  FlowchartViewModel.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 29.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

enum FlowchartViewModelItemType {
    case question
    case answer
    case function
    case description
    case showDescription
    case footer
}

protocol FlowchartViewModelItem: ViewModelItem {
    var type: FlowchartViewModelItemType { get }
}

protocol FlowchartViewModelMarkdownItem: FlowchartViewModelItem {
    var markdownString: NSAttributedString { get }
    var renderedString: NSAttributedString? { get set }
}

extension FlowchartViewModelMarkdownItem {
    
    var reuseIdentifier: String {
        return "MarkdownCell"
    }
    
    var selectionHandler: (() -> Void)? {
        return nil
    }
}

protocol FlowchartViewModelActionItem: class, FlowchartViewModelItem {
    var selectionHandler: (() -> Void)? { get set }
}

extension FlowchartViewModelActionItem {
    
    var reuseIdentifier: String {
        return "ActionCell"
    }
}

final class FlowchartViewModelQuestionItem: FlowchartViewModelMarkdownItem {
    
    var type: FlowchartViewModelItemType {
        return .question
    }
    
    var markdownString: NSAttributedString
    var renderedString: NSAttributedString?
    
    init(markdownString: NSAttributedString) {
        self.markdownString = markdownString
    }
}

final class FlowchartViewModelAnswerItem: FlowchartViewModelActionItem {
    
    var type: FlowchartViewModelItemType {
        return .answer
    }
    
    var answer: Endpoint
    var endpoint: SchemaEndpoint
    
    var selectionHandler: (() -> Void)?
    
    init(answer: Endpoint, endpoint: SchemaEndpoint) {
        self.answer = answer
        self.endpoint = endpoint
    }
}

final class FlowchartViewModelFunctionItem: FlowchartViewModelActionItem {
    
    var type: FlowchartViewModelItemType {
        return .function
    }
    
    var selectionHandler: (() -> Void)?
    
    var action: Action
    
    init(action: Action) {
        self.action = action
    }
}

final class FlowchartViewModelDescriptionItem: FlowchartViewModelMarkdownItem {
    
    var type: FlowchartViewModelItemType {
        return .description
    }
    
    var markdownString: NSAttributedString
    var renderedString: NSAttributedString?
    
    init(markdownString: NSAttributedString) {
        self.markdownString = markdownString
    }
    
    var status: Bool = false
}

final class FlowchartViewModelShowDescriptionItem: FlowchartViewModelItem {
    
    var reuseIdentifier: String {
        return "ShowDescriptionCell"
    }
    
    var type: FlowchartViewModelItemType {
        return .showDescription
    }
    
    var status: Bool = false
}

final class FlowchartViewModelFooterItem: FlowchartViewModelItem {
    
    var reuseIdentifier: String {
        return "FlowchartFooterCell"
    }
    
    var type: FlowchartViewModelItemType {
        return .footer
    }
}

final class FlowchartViewModel: NSObject {
    
    var items = [FlowchartViewModelItem]()
    var final: Bool = false
    
    init(flowchart: Flowchart, questionNum: Int) {
        
        super.init()
        
        let question = FlowchartViewModelQuestionItem(markdownString: NSAttributedString(string: flowchart.views[questionNum].message))
        
        items.append(question)
        
        if flowchart.views[questionNum].final == false {
            var row = 0
            for answer in flowchart.views[questionNum].endpoints {
                let endpoint = flowchart.schema[questionNum].endpoints[row]
                items.append(FlowchartViewModelAnswerItem(answer: answer, endpoint: endpoint))
                row += 1
            }
        } else {
            final = true
        }
        
        if let actions = flowchart.views[questionNum].actions {
            for action in actions {
                items.append(FlowchartViewModelFunctionItem(action: action))
            }
        }
        
        if flowchart.views[questionNum].description != "" {
            let description = FlowchartViewModelDescriptionItem(markdownString: NSAttributedString(string: flowchart.views[questionNum].description))
            
            items.append(description)
            items.append(FlowchartViewModelShowDescriptionItem())
        }
        
        if final {
            items.append(FlowchartViewModelFooterItem())
        }
    }
    
    func changeStatus() {
        
        for item in items {
            
            if let item = item as? FlowchartViewModelDescriptionItem {
                item.status = !item.status
            }
            
            if let item = item as? FlowchartViewModelShowDescriptionItem {
                item.status = !item.status
            }
        }
    }
    
    func getStatus() -> Bool {
        
        for item in items {
            
            if let item = item as? FlowchartViewModelDescriptionItem {
                return item.status
            }
            
            if let item = item as? FlowchartViewModelShowDescriptionItem {
                return item.status
            }
        }
        
        return false
    }
}
