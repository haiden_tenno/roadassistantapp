//
//  Flowchart.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 17.06.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

struct Flowchart: Codable {
    let views: [View]
    let initial: Int
    let schema: [Schema]
}

struct View: Codable {
    let message: String
    let final: Bool
    let description: String
    let endpoints: [Endpoint]
    let actions: [Action]?
}

struct Endpoint: Codable {
    let label: String
}

struct Action: Codable {
    
    private enum CodingKeys: String, CodingKey {
        case id
        case substituteMessage = "substitution-text"
        case src
    }
    
    let id: String
    let substituteMessage: String
    let src: String?
}

struct Schema: Codable {
    let endpoints: [SchemaEndpoint]
}

struct SchemaEndpoint: Codable {
    let to: Int
}
