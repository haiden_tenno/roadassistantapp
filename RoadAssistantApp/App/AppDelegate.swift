//
//  AppDelegate.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 17.06.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit
import GSMessages

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private let userDefaults = UserDefaults.standard
    private let flowchartPersistenceService = FlowchartServiceAssembly.pesristenceService
    private let flowchartNetworkService = FlowchartServiceAssembly.backgroundNetworkService
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        registerUserDefaults()
        
        setupGSMessage()
        
        flowchartNetworkService.delegates.addDelegate(delegate: self)
        
        application.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        #if DEBUG
        print("Background fetch running")
        #endif
        
        if userDefaults.autoUpdatesStatus {
            flowchartNetworkService.requestVersion()
        }
        
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {

    }
    
    func registerUserDefaults() {
        
        userDefaults.register(defaults: [Config.UserDefaults.autoUpdates : true])
        userDefaults.register(defaults: [Config.UserDefaults.version : Config.BundleFile.version])
        userDefaults.register(defaults: [Config.UserDefaults.timestamp : Config.BundleFile.timestamp])
        userDefaults.register(defaults: [Config.UserDefaults.date: Config.BundleFile.date])
    }
    
    func setupGSMessage() {
        
        GSMessage.font = .footnote
        GSMessage.successBackgroundColor = UIColor(white: 0.0, alpha: 0.8)
    }
    
}

extension AppDelegate: FlowchartNetworkServiceDelegate {
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad version: String, timestamp: Int, size: Int) {
        
        if TimestampHelper().checkIfDeviceTimestampOlder(than: timestamp) {
            flowchartNetworkService.requestFlowchart()
        }
    }
    
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String) {
        
        flowchartPersistenceService.store(flowchart: flowchart, version: version, timestamp: timestamp, date: date)
    }
}
