//
//  Config.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 17.06.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

enum Config {
    
    enum Network {
        static let serverURL = "http://188.166.180.165/actual"
        static let timeoutInterval: Double = 10
    }
    
    enum StoredFile {
        static let fileName = "flowchart"
        static let fileType = "json"
        static var fullFileName: String {
            return fileName + "." + fileType
        }
    }
    
    enum BundleFile {
        static let version = "0.00-bundle"
        static let timestamp = 0
        static let date = "01.01.2018"
    }
    
    enum HttpHeaders {
        static let version = "version"
        static let timestamp = "timestamp"
        static let size = "Content-Length"
        static let lastModified = "Last-Modified"
    }
    
    enum StoryboardIdentifiers {
        static let storyboard = "Main"
        static let settingsTVC = "SettingsTableViewController"
        static let flowchartTVC = "FlowchartTableViewController"
        static let updateVC = "UpdateViewController"
        static let mainMenu = "MainMenu"
        static let splashNavigationVC = "SplashNavigationController"
        static let download = "FlowchartDownloadViewController"
    }
    
    enum UserDefaults {
        static let version = "RoadAssistantVersion"
        static let timestamp = "RoadAssistantTimestamp"
        static let autoUpdates = "RoadAssistantAutoUpdates"
        static let storedFlowchartVersion = "RoadAssistantStoredFlowchartVersion"
        static let storedFlowchartTimestamp = "RoadAssistantStoredFlowchartTimestamp"
        static let date = "RoadAssistantDateOfBase"
    }
    
    enum Strings {
        
        enum FlowchartNetworkService {
            static let serverDateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
            static let deviceDateFormat = "dd.MM.yyyy"
        }
        
        static let footerLabel = "Это приложение сделано дромом\nдля вашей безопасности"
    }
    
    static let itemsToShare = ["https://www.drom.ru"]
    
    static let otherApps = [OtherApp(title: "Дром Авто", info: "Покупайте и продавайте автомобили, создавайте подписки", logoFileName: "dromAutoIOsAppIcon", url: "https://itunes.apple.com/ru/app/%D0%B4%D1%80%D0%BE%D0%BC-%D0%B0%D0%B2%D1%82%D0%BE-%D1%86%D0%B5%D0%BD%D1%8B-%D0%BD%D0%B0-%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D1%8B/id1043371985?mt=8"), OtherApp(title: "Дром База", info: "Покупайте и продавайте запчасти, авто- и мототовары, спецтехнику и водную технику", logoFileName: "dromBaseIOsAppIcon", url: "https://itunes.apple.com/ru/app/%D0%B4%D1%80%D0%BE%D0%BC-%D0%B1%D0%B0%D0%B7%D0%B0/id1082252059?mt=8")]
}
