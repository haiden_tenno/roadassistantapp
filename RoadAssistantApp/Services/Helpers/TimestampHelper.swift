//
//  TimestampHelper.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 18.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

final class TimestampHelper {
 
    private let userDefaults = UserDefaults.standard
    
    func checkIfDeviceTimestampOlder(than timestamp: Int) -> Bool {
        
        guard let deviceTimestamp = userDefaults.appTimestamp else { return true }
        
        if deviceTimestamp < timestamp {
            return true
        } else {
            return false
        }
    }
}
