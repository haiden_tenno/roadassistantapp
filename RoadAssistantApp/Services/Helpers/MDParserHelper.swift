//
//  MDParserHelper.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 27/09/2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import UIKit
import CDMarkdownKit

final class MDParserHelper {
    
    private let markdownParser = CDMarkdownParser(font: UIFont.body, boldFont: UIFont.headline, italicFont: UIFont.body, fontColor: UIColor.blackOne, backgroundColor: UIColor.clear)
    
    init() {
        
        markdownParser.image.size = CGSize(width: UIScreen.main.bounds.width, height: 50)
        markdownParser.header.font = .title2
    }
    
    func parse(_ markdown: NSAttributedString) -> NSAttributedString {
        return markdownParser.parse(markdown)
    }
}
