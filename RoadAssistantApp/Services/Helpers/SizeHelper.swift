//
//  SizeHelper.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 12.09.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

final class SizeHelper {
    
    func getStringSize(from bytesCount: Int) -> String {
        
        var updateSizeFloat = Float(bytesCount)
        var repeatCount = 0
        
        while updateSizeFloat > 1024 {
            updateSizeFloat = updateSizeFloat / 1024
            repeatCount += 1
        }
        
        let updateSizeStr = String(format: "%.1f", updateSizeFloat)

        var ending: String
        
        switch repeatCount {
        case 0:
            ending = "байт"
        case 1:
            ending = "кБ"
        case 2:
            ending = "МБ"
        case 3:
            ending = "ГБ"
        default:
            ending = "?"
        }
        
        let stringSize = "\(updateSizeStr)\(ending)"
        
        return stringSize
    }
    
    func getLoadedPercent(downloaded bytes: Int, outOf expectedBytes: Int) -> Float {
        
        let percentageDownloaded = Float(bytes) / Float(expectedBytes)
        
        return percentageDownloaded
    }
}
