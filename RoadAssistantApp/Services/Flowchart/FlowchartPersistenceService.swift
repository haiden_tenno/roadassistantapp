//
//  FlowchartPersistenceService.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 19.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

protocol FlowchartPersistenceService {
    func loadFlowchart() -> Flowchart
    func store(flowchart: Flowchart, version: String, timestamp: Int, date: String)
    func removeUpdates()
    func fileExists() -> Bool
}

final class FlowchartPersistenceServiceImplementation: FlowchartPersistenceService {
    
    private let userDefaults: UserDefaults
    private let bundle: Bundle
    
    var flowchart: Flowchart?
    
    init(userDefaults: UserDefaults, bundle: Bundle) {
        
        self.userDefaults = userDefaults
        self.bundle = bundle
    }
    
    func loadFlowchart() -> Flowchart {
        
        if flowchart != nil {
            return flowchart!
        } else {
            
            var flowch: Flowchart
            
            guard let storedFlowchartTimestamp = userDefaults.storedFlowchartTimestamp else {
                return loadFlowchartFromBundle()
            }
            
            if storedFlowchartTimestamp < Config.BundleFile.timestamp {
                return loadFlowchartFromBundle()
            }
            
            if Storage.fileExists(Config.StoredFile.fullFileName, in: .documents) {
                do {
                    flowch = try Storage.retrieve(Config.StoredFile.fullFileName, from: .documents, as: Flowchart.self)
                } catch  {
                    return loadFlowchartFromBundle()
                }
            } else {
                userDefaults.storedFlowchartVersion = nil
                userDefaults.storedFlowchartTimestamp = nil
                return loadFlowchartFromBundle()
            }
            
            userDefaults.appVersion = userDefaults.storedFlowchartVersion
            userDefaults.appTimestamp = userDefaults.storedFlowchartTimestamp
            return flowch
        }
    }
    
    private func loadFlowchartFromBundle() -> Flowchart {
        
        var flowch: Flowchart
        
        guard let filepath = bundle.path(forResource: Config.StoredFile.fileName, ofType: Config.StoredFile.fileType)  else {
            fatalError("Invalid bundle flowchart")
        }
        
        do {
            let contents = try String(contentsOfFile: filepath)
            
            guard let data = contents.data(using: .utf8) else {
                fatalError("Invalid bundle flowchart")
            }
            
            flowch = try JSONDecoder().decode(Flowchart.self, from: data)
        } catch {
            fatalError("Invalid bundle flowchart")
        }
        
        userDefaults.dateOfBase = Config.BundleFile.date
        userDefaults.appVersion = Config.BundleFile.version
        userDefaults.appTimestamp = Config.BundleFile.timestamp
        
        return flowch
    }
    
    func store(flowchart: Flowchart, version: String, timestamp: Int, date: String) {
        
        self.flowchart = flowchart
        
        Storage.store(flowchart, to: .documents, as: Config.StoredFile.fullFileName)
        
        userDefaults.storedFlowchartVersion = version
        userDefaults.storedFlowchartTimestamp = timestamp
        userDefaults.appVersion = version
        userDefaults.appTimestamp = timestamp
        userDefaults.dateOfBase = date
    }
    
    func removeUpdates() {
        
        self.flowchart = loadFlowchartFromBundle()
        
        Storage.remove(Config.StoredFile.fullFileName, from: .documents)
    }
    
    func fileExists() -> Bool {
        
        if Storage.fileExists(Config.StoredFile.fullFileName, in: .documents) {
            return true
        } else {
            return false
        }
    }
}
