//
//  FlowchartNetworkService.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 19.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

protocol FlowchartNetworkService {
    var delegates: MulticastDelegate<FlowchartNetworkServiceDelegate> { get set }
    func requestVersion()
    func requestFlowchart()
    func cancelTask()
    func hasActiveTask() -> Bool
}

protocol FlowchartNetworkServiceDelegate: class {
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad version: String, timestamp: Int, size: Int)
    func flowchartNetworkService(_ service: FlowchartNetworkService, didReceive error: Error)
    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String)
    func flowchartNetworkService(_ service: FlowchartNetworkService, downloaded downloadedBytes: Int, outOf expectedBytes: Int)
}

extension FlowchartNetworkServiceDelegate {

    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad flowchart: Flowchart, version: String, timestamp: Int, date: String) {}

    func flowchartNetworkService(_ service: FlowchartNetworkService, didLoad version: String, timestamp: Int, size: Int) {}

    func flowchartNetworkService(_ service: FlowchartNetworkService, didReceive error: Error) {
        #if DEBUG
        print("ERROR \(error)")
        #endif
    }

    func flowchartNetworkService(_ service: FlowchartNetworkService, downloaded downloadedBytes: Int, outOf expectedBytes: Int) {}
}

enum FlowchartNetworkManagerStatus {
    case standart
    case background
}

enum FlowchartNetworkServiceError: Error {
    case responseError
    case httpStatusError
    case parseError
}

final class FlowchartNetworkServiceImplementation: NSObject, FlowchartNetworkService {

    private let url = URL(string: Config.Network.serverURL)!
    private let dateFormatter = DateFormatter()

    private var request: URLRequest {
        return URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Config.Network.timeoutInterval)
    }
    private var task: URLSessionTask?
    private var lastLoadedPercent: Float = 0.0

    var urlSession: URLSession!
    var delegates = MulticastDelegate<FlowchartNetworkServiceDelegate>()

    private func resetVars() {

        lastLoadedPercent = 0.0
        task = nil
    }

    func hasActiveTask() -> Bool {

        guard task != nil else { return false }
        return true
    }

    func requestVersion() {

        var request = self.request
        request.httpMethod = "HEAD"

        task = urlSession.downloadTask(with: request)

        task!.resume()
    }

    func requestFlowchart() {

        var request = self.request
        request.httpMethod = "GET"

        task = urlSession.downloadTask(with: request)

        task!.resume()
    }

    func cancelTask() {

        task?.cancel()
        resetVars()
    }

    private func checkStatusCode(httpResponse: HTTPURLResponse) -> Error? {

        if (200...299).contains(httpResponse.statusCode) {
            return nil
        }
        return FlowchartNetworkServiceError.httpStatusError
    }

    private func handleHeaders(service: FlowchartNetworkServiceImplementation, response: URLResponse?) throws -> (timestamp: Int, version: String, size: Int, date: String) {

        guard let httpResponse = response as? HTTPURLResponse else {
            throw FlowchartNetworkServiceError.responseError
        }

        let statusError = service.checkStatusCode(httpResponse: httpResponse)

        guard statusError == nil else {
            throw statusError!
        }

        guard let lastModified = httpResponse.allHeaderFields[Config.HttpHeaders.lastModified] as? String else {
            throw FlowchartNetworkServiceError.responseError
        }

        dateFormatter.dateFormat = Config.Strings.FlowchartNetworkService.serverDateFormat

        //dateFormatter.locale = Locale(identifier: "en_US_POSIX")

        let lastModifiedDate = dateFormatter.date(from: lastModified)

        dateFormatter.dateFormat = Config.Strings.FlowchartNetworkService.deviceDateFormat

        let date = dateFormatter.string(from: lastModifiedDate!)

        guard let version = httpResponse.allHeaderFields[Config.HttpHeaders.version] as? String else {
            throw FlowchartNetworkServiceError.responseError
        }

        guard let stringTimestamp = httpResponse.allHeaderFields[Config.HttpHeaders.timestamp] as? String else {
            throw FlowchartNetworkServiceError.responseError
        }

        guard let timestamp = Int(stringTimestamp) else {
            throw FlowchartNetworkServiceError.responseError
        }

        guard let stringSize = httpResponse.allHeaderFields[Config.HttpHeaders.size] as? String else {
            throw FlowchartNetworkServiceError.responseError
        }

        guard let size = Int(stringSize) else {
            throw FlowchartNetworkServiceError.responseError
        }

        return (timestamp, version, size, date)
    }
}

extension FlowchartNetworkServiceImplementation: URLSessionDownloadDelegate {

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {

        guard let response = downloadTask.response else { return }

        let headers: (timestamp: Int, version: String, size: Int, date: String)

        do {
            headers = try handleHeaders(service: self, response: response)
        }
        catch let err {
            delegates.invoke {
                $0.flowchartNetworkService(self, didReceive: err)
            }

            resetVars()
            return
        }

        if downloadTask.currentRequest?.httpMethod == "HEAD" {

            let expectedContentLength = Int(response.expectedContentLength)
            delegates.invoke {
                $0.flowchartNetworkService(self, didLoad: headers.version, timestamp: headers.timestamp, size: expectedContentLength)
            }
            #if DEBUG
            print("Success pulling version")
            #endif
            resetVars()
        } else {
            do {
                let data = try Data(contentsOf: location)
                let flowch = try JSONDecoder().decode(Flowchart.self, from: data)

                delegates.invoke {
                    $0.flowchartNetworkService(self, didLoad: flowch, version: headers.version, timestamp: headers.timestamp, date: headers.date)
                }
                #if DEBUG
                print("Success pulling json")
                #endif
            } catch {
                delegates.invoke {
                    $0.flowchartNetworkService(self, didReceive: FlowchartNetworkServiceError.parseError)
                }
            }
            resetVars()
        }
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {

        let percentageDownloaded = SizeHelper().getLoadedPercent(downloaded: Int(totalBytesWritten), outOf: Int(totalBytesExpectedToWrite))

        if (percentageDownloaded - lastLoadedPercent) > 0.05 {
            delegates.invoke {
                $0.flowchartNetworkService(self, downloaded: Int(totalBytesWritten), outOf: Int(totalBytesExpectedToWrite))
            }

            lastLoadedPercent = percentageDownloaded

            #if DEBUG
            print("Downloading: \(percentageDownloaded * 100)")
            #endif
        }
    }

}

extension FlowchartNetworkServiceImplementation: URLSessionDelegate {

    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        #if DEBUG
        print("End of background events")
        #endif
    }
}

extension FlowchartNetworkServiceImplementation: URLSessionTaskDelegate {

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {

        guard let error = error else { return }
        delegates.invoke {
            $0.flowchartNetworkService(self, didReceive: error)
        }
        resetVars()
    }
}
