//
//  FlowchartServiceAssembly.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 25.07.2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

final class FlowchartServiceAssembly {
    
    static let networkService = FlowchartServiceAssembly().makeFlowchartNetworkService(delegate: nil, flag: .standart)
    static let backgroundNetworkService = FlowchartServiceAssembly().makeFlowchartNetworkService(delegate: nil, flag: .background)
    static let pesristenceService = FlowchartServiceAssembly().makeFlowchartPersistenceService()
    
    //Сетевой сервис через URLSession
    func makeFlowchartNetworkService(delegate: FlowchartNetworkServiceDelegate?, flag: FlowchartNetworkManagerStatus) -> FlowchartNetworkService {

        let urlSession: URLSession

        let networkService = FlowchartNetworkServiceImplementation()

        switch flag {

        case .standart:

            urlSession = URLSession(configuration: .default, delegate: networkService, delegateQueue: nil)

        case .background:

            urlSession = URLSession(configuration: .background(withIdentifier: "Background"), delegate: networkService, delegateQueue: nil)
        }

        networkService.urlSession = urlSession

        guard let delegate = delegate else { return networkService }

        networkService.delegates.addDelegate(delegate: delegate)

        return networkService
    }
    
    func makeFlowchartPersistenceService() -> FlowchartPersistenceService {

        return FlowchartPersistenceServiceImplementation(userDefaults: UserDefaults.standard, bundle: Bundle.main)
    }
}
