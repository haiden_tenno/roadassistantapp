//
//  FlowchartRenderer.swift
//  RoadAssistantApp
//
//  Created by Петр Тартынских  on 08/10/2018.
//  Copyright © 2018 Петр Тартынских . All rights reserved.
//

import Foundation

final class FlowchartRenderer {
    
    private let parser = MDParserHelper()
    
    func render(flowchartViewModel: FlowchartViewModel, completion: @escaping (FlowchartViewModel) -> Void) {
        
        let queue = DispatchQueue(label: "RenderQueue", qos: .userInteractive, attributes: .concurrent)
        
        queue.async { [weak self] in
            
            guard let strongSelf = self else { return }
            
            for item in flowchartViewModel.items {
                
                guard var markdownItem = item as? FlowchartViewModelMarkdownItem else { continue }
                
                markdownItem.renderedString = strongSelf.parser.parse(markdownItem.markdownString)
            }
            
            completion(flowchartViewModel)
        }
    }
}
